#!/usr/bin/python3

from argparse import ArgumentParser, FileType

parser = ArgumentParser()
parser.add_argument("solfile",type=FileType("r"),nargs='+')
parser.add_argument("--suppress-ok",action="store_true",help="Only output files affected by the bug.")
args = parser.parse_args()
for fil in args.solfile:
    for line in fil:
        if line.startswith('#'):
            s = (line.split('# Objective value ='))
            if len(s) > 1:
                oldobj = int(float(s[1].strip()))
                newobj = int(round(float(s[1].strip())))
                nok = (oldobj!=newobj)
                if not args.suppress_ok or nok:
                    print("{file} : {nok}".format(file=fil.name, nok= "BUG!" if nok else "OK"))